/////////////////////////////////////////////////////////////////////////////////////////////
//
// io-driver-localstorage
//
//    IO Driver for accessing the runtimes localstorage.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Constants
//
/////////////////////////////////////////////////////////////////////////////////////////////
var PROTOCOL_LOCALSTORAGE = "ls";
var MAX_SIZE =              "5242880"; //5MB max for local storage:
                                       //https://demo.agektmr.com/storage/

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error =    require("error");
var host =     require("host");
var type =     require("type");
var ioAccess = require("io-access");
var ioURI =    require("io-uri");
var ioStream = require("io-stream");

function requireDependencies() {
    if (typeof localStorage === "undefined") {
        throw new Error(host.ERROR_RUNTIME_NOT_SUPPORTED, "The runtime does not support local storage.");
    }
}
function getItem(path) {
    return localStorage.getItem(PROTOCOL_LOCALSTORAGE + ":///" + (path.indexOf("/") == 0? path.substr(1) : path));
}
function setItem(path, value) {
    return localStorage.setItem(PROTOCOL_LOCALSTORAGE + ":///" + (path.indexOf("/") == 0? path.substr(1) : path), value);
}
function removeItem(path) {
    return localStorage.removeItem(PROTOCOL_LOCALSTORAGE + ":///" + (path.indexOf("/") == 0? path.substr(1) : path));
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// StreamFileSystem Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function StreamLocalStorage(buffer, key, access) {
    var own = this;

    var written = false;
    var closed = false;

    this.getName = function() {
        if (key && key.lastIndexOf("/") >= 0) {
            return key.substr(key.lastIndexOf("/"));
        }
        else {
            return key;
        }
    };
    this.getLength = function() {
        return new Promise(function(resolve, refuse) {
            requireDependencies();

            if (closed) {
                refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                return;
            }
            resolve(buffer.length);
        });
    };
    this.read = function (len, position) {
        if (!position) {
            position = 0;
        }

        return new Promise(function(resolve, refuse) {
            requireDependencies();

            if (closed) {
                refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            if (len == null) {
                return own.getLength().then(function (length) {
                    doRead(length - position);
                }).catch(refuse);
            }
            else {
                doRead(len);
            }

            function doRead(len) {
                if (len == buffer.length) {
                    resolve(buffer);
                }
                else {
                    var nBuf = buffer.subarray(position, position + len);
                    position += len;
                    resolve(nBuf);
                }
            }
        });
    };
    this.write = function(data, position) {
        if (!position) {
            position = 0;
        }
        
        return new Promise(function(resolve, refuse) {
            requireDependencies();

            if (closed) {
                refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            if (data == null) {
                resolve();
                return;
            }
            if (!((typeof Buffer != "undefined" && data instanceof Buffer) || (typeof Uint8Array != "undefined" && data instanceof Uint8Array) || type.isString(data))) {
                refuse(new Error(Error.ERROR_INVALID_PARAMETER, "Invalid parameter 'data'. The parameter should be of type 'Buffer', 'Uint8Array' or 'String'."));
                return;
            }
            if (!type.isFunction(data.toString)) {
                refuse(new Error(Error.ERROR_INVALID_PARAMETER, "Invalid parameter 'data'. The parameter should have the toString() function."));
                return;
            }
            var newData;
            var newB;
            var newLength;

            newData = data instanceof Uint8Array? data : data.toUint8Array();
            newLength = position + newData.length;
            if (access == access.ACCESS_MODIFY && newLength < buffer.length) {
                newLength = buffer.length;
            }
            newB = new Uint8Array(newLength);
            //only write previous contents if not access is modify and data is not text, or has been written once (overwrite)
            if (access == access.ACCESS_MODIFY || written) {
                newB.set(buffer, 0);
            }
            else if (!written && type.isString(data)) {
                newB.set(" ".repeat(position).toUint8Array(), 0);
            }
            newB.set(newData, position);

            buffer = newB;
            var bfrStr = buffer.toString();
            setItem(key, bfrStr);
            written = true;
            resolve();
        });
    };
    this.close = function (remove) {
        return new Promise(function(resolve, refuse) {
            requireDependencies();

            if (closed) {
                refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            buffer = null;
            closed = true;

            if (remove) {
                removeItem(key);
            }

            resolve();
        });
    };
};
StreamLocalStorage.open = function(uri, opt_access, opt_create) {
    return new Promise(function(resolve, reject) {
        requireDependencies();

        var buffer;
        var data = getItem(uri.toString());

        if (data) {
            buffer = new Uint8Array(data.length);
            Array.prototype.forEach.call(data, function (ch, i) {
                buffer[i] = ch.charCodeAt(0);
            });

            resolve(new StreamLocalStorage(buffer, uri.toString(), opt_access));
            return;
        }
        else if (opt_access && opt_access != access.ACCESS_READ) {
            setItem(uri.toString(), "");
            resolve(new StreamLocalStorage(new Uint8Array(0), uri.toString(), opt_access));
            return;
        }

        reject(new Error(ioURI.ERROR_NO_ENTRY, ""));
    });
};
StreamLocalStorage.prototype = ioStream;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// URI Handler
//
/////////////////////////////////////////////////////////////////////////////////////////////
var mod = {};
mod.parse = function(uri) {
    if(uri && type.isString(uri)) {
        if (uri.length >= 5 && uri.substr(0,5) == PROTOCOL_LOCALSTORAGE + "://") {
            return new ioURI(uri, self);
        }
    }
};
mod.open = function(uri, opt_access, opt_create) {
    if (uri && type.isString(uri)) {
        uri = mod.parse(uri);
    }
    else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
        uri = null;
    }
    if (!uri) {
        throw new Error(ioURI.ERROR_INVALID_URI, "");
    }
    return StreamLocalStorage.open(uri.path, opt_access, opt_create);
};
mod.exists = function(uri) {
    return new Promise(function(resolve, refuse) {
        requireDependencies();

        if (uri && type.isString(uri)) {
            uri = mod.parse(uri);
        }
        else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        if (uri.path == "/") {
            resolve(ioURI.ENTRY_DIRECTORY);
            return;
        }
        var val = getItem(uri.path);
        if (val == null) {
            resolve();
            return;
        }
        resolve(ioURI.ENTRY_FILE);
    });

};
mod.delete = function(uri) {
    return new Promise(function(resolve, refuse) {
        requireDependencies();

        if (uri && type.isString(uri)) {
            uri = mod.parse(uri);
        }
        else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        if (!uri) {
            reject(new Error(ioURI.ERROR_INVALID_URI, ""));
            return;
        }
        var val = null;
        if (uri) {
            val = getItem(uri.path);
        }
        if (val == null) {
            refuse(new Error(ioURI.ERROR_NO_ENTRY, ""), "The file or directory does not exist local storage.");
            return;
        }
        removeItem(uri.path);
        resolve();
    });

};
mod.toString = function(uri, opt_format) {
    if (uri && type.isString(uri)) {
        uri = mod.parse(uri);
    }
    else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
        uri = null;
    }
    if (!uri) {
        return "";
    }
    switch (opt_format) {
        default:
            return uri.toString();
    }
};
mod.StreamLocalStorage = StreamLocalStorage;
mod.PROTOCOL_LOCALSTORAGE = PROTOCOL_LOCALSTORAGE;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Register LS Protocol Handler
//
/////////////////////////////////////////////////////////////////////////////////////////////
ioURI.protocols.register(mod, PROTOCOL_LOCALSTORAGE);

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = mod;